// @flow

import { CONFIG } from '../constants/api';

const { apiKey, baseUrl } = CONFIG;
const key = apiKey || ''; // work around flow complaining

export function getCharacters(offset: ?string) {
  if (!offset) {
    return fetch(`${baseUrl}characters?limit=12&apikey=${key}`).then(response =>
      response.json()
    );
  } else {
    return fetch(
      `${baseUrl}characters?limit=12&offset=${offset}&apikey=${key}`
    ).then(response => response.json());
  }
}

export function getCharacter(name: string) {
  const heroName = encodeURIComponent(name);

  return fetch(`${baseUrl}characters?name=${heroName}&apikey=${key}`)
    .then(response => {
      return response.json();
    })
    .catch(error => Promise.reject(error));
}
