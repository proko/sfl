import React from 'react';
import { render } from 'react-testing-library';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import heroes from '../reducers/heroes';

export const customRender = (
  ui,
  { initialState, store = createStore(heroes, initialState) } = {}
) => {
  return {
    ...render(<Provider store={store}>{ui}</Provider>),
    store
  };
};

export * from 'react-testing-library';
export { customRender as render };
