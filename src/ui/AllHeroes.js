// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { css } from 'emotion';
import { getCharacters } from '../utils/api';
import { loadHeroes } from '../actions/heroes';

import Hero from './Hero';
import { COLORS } from '../constants/styles';

const heroGrid = css`
  display: grid;
  grid-gap: 1rem;
  padding: 0;
  grid-template-columns: repeat(auto-fit, minmax(180px, 1fr));
  list-style-type: none;
  margin: 0 auto;
`;

const heading = css`
  color: ${COLORS.white};
`;

type State = {
  heroes: Array<*>
};

type Props = {
  dispatch: any,
  loadedHeroes: Array<*>
};

class AllHeroes extends Component<Props, State> {
  state = {
    heroes: this.props.loadedHeroes || []
  };

  componentDidMount() {
    if (!this.state.heroes.length) {
      getCharacters().then((data: any) => {
        this.setState({ heroes: data.data.results });
        this.props.dispatch(loadHeroes(data.data.results));
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.loadedHeroes !== prevProps.loadedHeroes) {
      this.setState({
        heroes: this.props.loadedHeroes
      });
    }
  }

  render() {
    return (
      <div>
        <h3 className={heading}>All Heroes</h3>
        <ul className={heroGrid}>
          {this.state.heroes.map(hero => (
            <li key={hero.id} data-testid="mrv-hero-item">
              <Hero hero={hero} />
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loadedHeroes: state.loadedHeroes
});

export default connect(mapStateToProps)(AllHeroes);
