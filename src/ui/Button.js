import React from 'react';
import { css } from 'react-emotion';

import { COLORS } from '../constants/styles';

const buttonStyles = css`
  color: ${COLORS.white};
  background-color: ${COLORS.red};
  border: none;
  cursor: pointer;
  padding: 0 1rem;
  position: relative;
  margin: 10px 1px;
  text-transform: uppercase;
  line-height: 3;
  vertical-align: middle;
  font-size: 1rem;
  display: inline-block;
  overflow: visible;
  outline: none;

  :hover {
    background-image: linear-gradient(to bottom, #ff0707, #a00000);
  }
`;

const Button = ({ disabled, children, ...props }) => {
  return (
    <button disabled={disabled} className={buttonStyles} {...props}>
      {children}
    </button>
  );
};

export default Button;
