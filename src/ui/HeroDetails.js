import React from 'react';
import { connect } from 'react-redux';
import styled, { css } from 'react-emotion';
import { Link } from '@reach/router';
import { COLORS, BREAKPOINTS } from '../constants/styles';

const content = css`
  @media (min-width: ${BREAKPOINTS.mq2}) {
    display: flex;
  }
`;

const heroImage = css`
  @media (min-width: ${BREAKPOINTS.mq2}) {
    width: 50%;
  }
`;

const descriptionText = css`
  font-size: 20px;
  padding: 0 1rem;
  line-height: 1.5;

  @media (min-width: ${BREAKPOINTS.mq2}) {
    width: 50%;
  }
`;

const Back = styled(Link)`
  display: inline-block;
  background-color: ${COLORS.white};
  font-size: 22px;
  padding: 1rem;
  text-decoration: none;
  color: ${COLORS.dark};
`;

const HeroDetails = ({ savedHero }) => {
  if (!savedHero) {
    return (
      <Back to="/">
        <span>&#8592;</span>
        Back
      </Back>
    );
  } else {
    const { name, description, thumbnail } = savedHero;
    return (
      <div>
        <Back to="/">
          <span>&#8592;</span>
          Back
        </Back>
        <h1>{name}</h1>
        <div className={content}>
          <div className={heroImage}>
            <img src={`${thumbnail.path}.${thumbnail.extension}`} alt={name} />
          </div>
          <div className={descriptionText}>
            {description ? (
              <p>{description}</p>
            ) : (
              <p>No description available</p>
            )}
          </div>
        </div>
      </div>
    );
  }
};

const mapStateToProps = state => ({
  savedHero: state.hero
});

export default connect(mapStateToProps)(HeroDetails);
