import React from 'react';
import { css } from 'emotion';

import { BREAKPOINTS } from '../constants/styles';

import logo from '../logo.svg';
import FoundHeroContainer from '../containers/FoundHeroContainer';
import AllHeroes from './AllHeroes';
import Search from './Search';
import MoreLoader from './MoreLoader';

const appHeader = css`
  display: flex;
  flex-direction: column;
  align-items: center;
  font-size: calc(10px + 2vmin);
  color: white;

  @media (min-width: ${BREAKPOINTS.mq2}) {
    flex-direction: row;
    justify-content: space-between;
  }
`;

const appLogo = css`
  width: 250px;
`;

const Home = () => {
  return (
    <>
      <header className={appHeader}>
        <img src={logo} className={appLogo} alt="logo" />
        <Search />
      </header>
      <FoundHeroContainer />
      <AllHeroes />
      <MoreLoader />
    </>
  );
};

export default Home;
