// @flow

import React from 'react';
import { css } from 'emotion';
import { COLORS, BREAKPOINTS } from '../constants/styles';

import type { Hero as MarvelHero } from '../types/hero';

const heroItem = css`
  background-color: ${COLORS.white};
  padding: 1rem;
  height: 100%;

  @media (min-width: ${BREAKPOINTS.mq2}) {
    max-width: 30vw;
  }
`;

const thumbnailWrapper = css`
  position: relative;
  padding-bottom: 100%;
  margin: 0;
`;

const thumbnail = css`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  object-fit: cover;
  object-position: left;
`;

const heroName = css`
  color: ${COLORS.dark};
  font-weight: bold;
  font-size: 1.2rem;
  word-break: break-word;
  hyphens: auto;

  @media (min-width: ${BREAKPOINTS.mq2}) {
    font-size: 1.6rem;
  }
`;

const Hero: MarvelHero = ({ hero }) => {
  const {
    name,
    thumbnail: { path, extension }
  } = hero;

  return (
    <div className={heroItem}>
      <figure className={thumbnailWrapper}>
        <img className={thumbnail} src={`${path}.${extension}`} alt={name} />
      </figure>
      <span className={heroName}>{name}</span>
    </div>
  );
};

export default Hero;
