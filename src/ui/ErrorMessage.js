import React from 'react';
import styled, { css } from 'react-emotion';
import { COLORS } from '../constants/styles';

const errorStyles = props => css`
  color: ${COLORS.red};
  font-size: ${props.size}px;
`;

const Error = styled('div')`
  ${errorStyles};
`;

const ErrorMessage = ({ size, children }) => {
  return <Error size={size}>{children}</Error>;
};

export default ErrorMessage;
