import React from 'react';
import { Link } from '@reach/router';
import { css } from 'emotion';
import { COLORS } from '../constants/styles';

import Hero from './Hero';
import ErrorMessage from './ErrorMessage';

const searchResult = css`
  border: 2px dashed ${COLORS.grey};
  padding: 2rem;
  margin-top: 1rem;
`;

const placeholder = css`
  color: ${COLORS.grey};
`;

const more = css`
  color: ${COLORS.white};
  text-decoration: none;
  border: 1px solid currentColor;
  padding: 5px 15px;
  display: inline-block;
  margin-top: 1rem;

  :hover {
    color: ${COLORS.red};
  }
`;

const FoundHero = ({ hero }) => {
  return (
    <div className={searchResult}>
      {!hero && <span className={placeholder}>Your Marvel hero</span>}
      {hero &&
        !hero.error && (
          <>
            <Link to={`/hero/${hero.id}`}>
              <Hero hero={hero} />
            </Link>
            <Link to={`/hero/${hero.id}`} className={more}>
              View Details <span>&#8594;</span>
            </Link>
          </>
        )}
      {hero &&
        hero.error && (
          <ErrorMessage size="22">
            <span>Sorry! We couldn't have a hero with that name yet.</span>
          </ErrorMessage>
        )}
    </div>
  );
};

export default FoundHero;
