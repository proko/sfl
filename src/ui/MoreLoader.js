import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getCharacters } from '../utils/api';
import { loadHeroes } from '../actions/heroes';

import Button from './Button';

class MoreLoader extends Component {
  state = {
    offset: this.props.loadedHeroes ? this.props.loadedHeroes.length : 0,
    loading: false
  };

  componentDidUpdate(prevProps) {
    if (this.props.loadedHeroes !== prevProps.loadedHeroes) {
      this.setState({
        offset: this.props.loadedHeroes.length
      });
    }
  }

  onLoadMore = () => {
    this.setState({
      loading: true
    });
    getCharacters(this.state.offset).then(data => {
      this.setState({
        loading: false
      });
      this.props.dispatch(loadHeroes(data.data.results));
    });
  };

  render() {
    return (
      <div>
        <Button onClick={this.onLoadMore} disabled={this.state.loading}>
          Load More
        </Button>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loadedHeroes: state.loadedHeroes
});

export default connect(mapStateToProps)(MoreLoader);
