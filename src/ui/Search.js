// @flow

import * as React from 'react';
import { css } from 'emotion';
import { connect } from 'react-redux';
import { getCharacter } from '../utils/api';
import { BREAKPOINTS } from '../constants/styles';
import { saveHero } from '../actions/heroes';

import Button from './Button';

import type { Dispatch } from '../types/redux';

const visuallyHidden = css`
  clip: rect(1px, 1px, 1px, 1px);
  height: 1px;
  overflow: hidden;
  position: absolute;
  white-space: nowrap;
  width: 1px;
`;

const heading = css`
  font-size: 28px;
  font-weight: bold;
  padding: 0 0.5rem;
`;

const container = css`
  align-self: stretch;

  @media (min-width: ${BREAKPOINTS.mq2}) {
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;

const searchForm = css`
  display: flex;
  align-items: center;
`;

const searchInput = css`
  flex: 1;
  font-size: 1rem;
  line-height: 3;
  padding: 0 10px;
  border: none;
`;

type State = {
  term: string
};

type Props = {
  dispatch: Dispatch
};

class Search extends React.Component<Props, State> {
  state = {
    term: ''
  };

  onChange = (event: SyntheticEvent<HTMLInputElement>) => {
    event.preventDefault();
    this.setState({ term: event.currentTarget.value });
  };

  onSubmit = (event: SyntheticEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (this.state.term) {
      getCharacter(this.state.term).then(data => {
        if (data.data.results.length) {
          this.props.dispatch(saveHero(data.data.results[0]));
        } else {
          this.props.dispatch(saveHero({ error: 'lala' }));
        }
      });
    }
  };

  render() {
    return (
      <div className={container}>
        <span className={heading}>Find your favourite Hero</span>
        <form
          data-testid="search-form"
          className={searchForm}
          onSubmit={this.onSubmit}
        >
          <label htmlFor="hero" className={visuallyHidden}>
            Hero Name
          </label>
          <input
            className={searchInput}
            type="text"
            id="hero"
            value={this.state.term}
            onChange={this.onChange}
          />
          <Button type="submit">Search</Button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  savedHero: state.hero
});

export default connect(mapStateToProps)(Search);
