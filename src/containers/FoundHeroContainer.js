import { connect } from 'react-redux';
import FoundHero from '../ui/FoundHero';

const mapStateToProps = (state, ownProps) => ({
  hero: state.hero
});

export default connect(mapStateToProps)(FoundHero);
