import React, { Component } from 'react';
import { css, injectGlobal } from 'emotion';
import { Router } from '@reach/router';
import { COLORS } from './constants/styles';

import Home from './ui/Home';
import HeroDetails from './ui/HeroDetails';

injectGlobal`
  * {
    box-sizing: border-box;
  }
  
  body {
    margin: 0;
    padding: 0;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
      "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
      sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    color: #fff;
    background-color: ${COLORS.dark};
  }

  img {
    max-width: 100%;
    height: auto;
  }
`;

const appContainer = css`
  min-height: 100vh;
  max-width: 1024px;
  margin: 0 auto;
  padding: 1rem 3rem;
`;

class App extends Component {
  render() {
    return (
      <div className={appContainer}>
        <Router>
          <Home path="/" />
          <HeroDetails path="hero/:heroId" />
        </Router>
      </div>
    );
  }
}

export default App;
