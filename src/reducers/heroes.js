// @flow
import type { Action, State } from '../types/redux';

const heroes = (state: State = { loadedHeroes: [] }, action: Action) => {
  switch (action.type) {
    case 'SAVE_HERO_DATA':
      return { ...state, hero: action.payload };
    case 'LOAD_HEROES':
      return {
        ...state,
        loadedHeroes: [...state.loadedHeroes, ...action.payload]
      };
    default:
      return state;
  }
};

export default heroes;
