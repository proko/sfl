// @flow

import type { Action } from '../types/redux';
import type { Hero } from '../types/hero';

export const saveHero: Action = (hero: Hero) => ({
  type: 'SAVE_HERO_DATA',
  payload: hero
});

export const loadHeroes: Action = (data: Array<Hero>) => ({
  type: 'LOAD_HEROES',
  payload: data
});
