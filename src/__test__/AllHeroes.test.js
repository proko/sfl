import React from 'react';
import { render, waitForElement } from '../utils/test-utils';
import AllHeroes from '../ui/AllHeroes';

describe('testing AllHeroes', () => {
  beforeEach(() => {
    fetch.resetMocks();
  });

  it('should call API for first 12 items', async () => {
    fetch.mockResponseOnce(
      JSON.stringify({
        data: {
          results: [
            {
              id: 1011334,
              name: '3-D Man',
              description: '',
              modified: '2014-04-29T14:18:17-0400',
              thumbnail: {
                path:
                  'http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784',
                extension: 'jpg'
              },
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1011334'
            },
            {
              id: 1017100,
              name: 'A-Bomb (HAS)',
              description:
                "Rick Jones has been Hulk's best bud since day one, but now he's more than a friend...he's a teammate! Transformed by a Gamma energy explosion, A-Bomb's thick, armored skin is just as strong and powerful as it is blue. And when he curls into action, he uses it like a giant bowling ball of destruction! ",
              modified: '2013-09-18T15:54:04-0400',
              thumbnail: {
                path:
                  'http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16',
                extension: 'jpg'
              },
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1017100'
            }
          ]
        }
      })
    );

    const { getByTestId } = render(<AllHeroes />);
    const greetingTextNode = await waitForElement(() =>
      getByTestId('mrv-hero-item')
    );

    expect(fetch.mock.calls.length).toEqual(1);
    expect(fetch.mock.calls[0][0]).toMatch('/characters?limit=12');
    expect(greetingTextNode.parentElement.children.length).toBe(2);
  });
});
