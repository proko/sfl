import React from 'react';
import { render, wait, fireEvent } from '../utils/test-utils';
import Search from '../ui/Search';

describe('testing Search', () => {
  beforeEach(() => {
    fetch.resetMocks();
  });

  it('calls marvel API with name parameter', async () => {
    fetch.mockResponseOnce(JSON.stringify({ data: '12345' }));

    const { getByLabelText, getByTestId } = render(<Search />);
    const searchInput = getByLabelText('Hero Name');
    fireEvent.change(searchInput, {
      target: { value: '3-D Man' }
    });

    expect(searchInput.value).toBe('3-D Man');

    fireEvent.submit(getByTestId('search-form'));

    expect(fetch.mock.calls.length).toEqual(1);
    expect(fetch.mock.calls[0][0]).toMatch('/characters?name=3-D%20Man');
  });
});
