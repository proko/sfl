export const COLORS = {
  white: '#FFFFFF',
  grey: '#CCCCCC',
  dark: '#282C34',
  red: '#F30A0A'
};

export const BREAKPOINTS = {
  mq1: '600px',
  mq2: '900px',
  mq3: '1200px'
};
