// @flow

import type ApiConfig from '../types/api';

export const CONFIG: ApiConfig = {
  baseUrl: 'https://gateway.marvel.com:443/v1/public/',
  apiKey: process.env.REACT_APP_API_KEY
};
