export type ApiConfig = {
  baseUrl: string,
  apiKey: ?string
};
