import type Hero from './hero';

export type State = {
  loadedHeroes: Array<Hero>,
  hero: ?Hero
};

export type SaveHeroAction = {
  type: 'SAVE_HERO_DATA',
  payload: Hero
};

export type LoadHerosAction = {
  type: 'LOAD_HEROES',
  payload: Array<Hero>
};

export type Action = SaveHeroAction | LoadHerosAction;

export type Dispatch = (action: Action | Array<Action>) => any;
