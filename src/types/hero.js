export type MVLImage = {
  path: string,
  extension: string
};

export type MVLUrl = {
  type: string,
  url: string
};

export type MVLResourceItem = {
  resourceURI: string,
  name: string,
  type: ?string
};

export type MVLResource = {
  available: number,
  collectionURI: string,
  items: Array<MVLResourceItem>,
  returned: number
};

export type Hero = {
  id: number,
  name: string,
  description: string,
  modified: string,
  thumbnail: MarvelImage,
  resourceURI: string,
  comics: MVLResource,
  series: MVLResource,
  stories: MVLResource,
  events: MVLResource,
  urls: Array<MVLUrl>
};
