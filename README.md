# Notes:
The result of this test is an app that I think covers the requirements and acceptance criteria even though the user experience is not great. I think this is because of the limitations of the API unless there is something I missed in the description of the test or the documentation of the API. I hope we will have the opportunity to discuss that in the future.

## Tooling (and sidenotes)
- Create React App to set it up for the sake of speed. 
- Redux just to cover data sharing. Possibly Context API could be enough for this test but for scalability Redux is better. Could have been integrated in more depth (eg. async actions)
- Flow. Again could be integrated deeper and try to achieve 100% coverage but because of lack of time I implemented it in a "demo" level
- [React Testing Library](https://github.com/kentcdodds/react-testing-library). I only tested the parameters used to call the API and some more random assertions. 
- [Emotion](https://emotion.sh/) for styles. I find it to be the most flexible and powerfull css-in-js solution and as far as I know it is the fastest one too.

## Usage
To run the app, clone the repo and from the root directory run the commands:

 `npm install`
 `npm start` 

### PS
I didn't work on the test having version control in mind, that's why there is only one huge commit in a single branch. Real life would be much different of course

---

# Frontend test

As a user we want to search any character of Marvel Universe and see some information as the photo or description

API: https://developer.marvel.com/

### Acceptance criteria

* The user should be able to search any character of MARVEL
* The results item should be display only photo and name by each character
* The results should be shown as a Pinterest layout, rectangles in a grid
* The user should be able to see the information of the character in a full page with a big photo and a clear description after a click over a character item
* The user should be close this full page and come back to results list

### Requirements

* You have to use the information of MARVEL API
* The application must be made with ReactJS
* The code and the organization of the modules must be clear, with a good separation by responsabilities
* Check the params required to call to the api with a test
* Create a document and descript the process (steps, reasoning) that you follow to build the application (your way of thinking) **only main points**

### Very important

* You will have **extra points** if you include good things as Jest, Eslint, Flow, Redux or another interesting library to do more reliable your code
* If we need to know something about how works your project please write it in the same document
* Please, if you have any blocker inconvenient to do this test tell us and we respond you ASAP


Thank you for your time and good luck !
